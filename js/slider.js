'use strict';

const IMAGES = [
  "https://www.pets4homes.co.uk/images/articles/771/large/cat-lifespan-the-life-expectancy-of-cats-568e40723c336.jpg",
  "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/800px-Cat03.jpg",
  "https://i.ytimg.com/vi/OqQPv78AMw0/maxresdefault.jpg"
];

const ELEMENT_NAME = 'imageSliderScript';

let iterator = 0;

window.onload = function() {
  let imageSliderScript = document.getElementById(ELEMENT_NAME);
  console.log(imageSliderScript.getAttribute("images"));
  let imageSlider = document.createElement('div');
  imageSlider.innerHTML = `
  <div class="imageSlider">
    <div id="sliderImageDiv">
      <img class="img-thumbnail" id="sliderImage">
    </div>
    <div id="imageSliderButtons">
      <button type="button" class="btn btn-primary" id="previousButton">Previous</button>
      <button type="button" class="btn btn-primary" id="nextButton">Next</button>
    </div>
  </div>`;

  imageSliderScript.parentNode.insertBefore(imageSlider, imageSliderScript);

  let image = document.getElementById("sliderImage");
  let previousButton = document.getElementById("previousButton");
  let nextButton = document.getElementById("nextButton");

  IMAGES.forEach((source, index) => {
    let i = new Image();
    if (index === 0)
      i.onload = () => {
        image.src = IMAGES[0];
      };
    i.src = source;
  });

  previousButton.addEventListener('click', event => {
    iterator--;
    if (iterator < 0)
      iterator = IMAGES.length - 1;
    image.src = IMAGES[iterator];
  });

  nextButton.addEventListener('click', event => {
    iterator++;
    if (iterator >= IMAGES.length)
      iterator = 0;
    image.src = IMAGES[iterator];
  });
}